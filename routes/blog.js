var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;
var _ = require('underscore');
var marked = require('marked');
marked.setOptions({
  highlight: function (code) {
    return require('highlight.js').highlightAuto(code).value;
  },
  langPrefix: "hljs-"
});

var options = {
  server: { poolSize: 5 }
};
mongoose.connect(process.env.MONGODB_URL, options);

var wtBlogSchema = mongoose.Schema({
  user: String,
  title: String,
  content: String,
  room: String,
  sasuga: Number,
  mode: String,
  rendered: String,
  deleted: Date,
  updated: {
    type: Date,
    default: Date.now
  }
});
var Blog = mongoose.model('wtBlog', wtBlogSchema);

//integration for Kato
function Notify(msg){

  if(process.env.KATO_ROOM_URL){
    var request = require('request');
    var options = {
      url: process.env.KATO_ROOM_URL,
      headers: {  'Content-Type': 'application/json' },
      json: true,
      body: JSON.stringify({ 
        "from": "IPL-WS",
        "renderer": "markdown",
		    "color": "black",
        "text": msg
      })
    };

    request.post(options, function(error, response, body){
      if (!error && response.statusCode == 200) {
        console.log(body.name);
      } else {
        console.log('error: '+ response.statusCode);
      }
    });
  }
}

module.exports = {
  index: function(req, res){
    var cond = {deleted: { $exists: false }};
    return Blog.find( cond ).sort({updated: -1}).exec(function(err, docs){
      var items = _.map(
        docs, function(item){
          var rendered;
          if(item.mode === "markdown"){
            rendered = marked(item.content);
          }else{
            rendered = item.content.replace(/\r?\n/g, "<br />");;
          }
          item["rendered"] = rendered;
          return item;
        }
      );
      res.json(items);
    });
  },
  sasuga: function(req, res){
    return Blog.findOne( {_id : new ObjectId(req.params.id)},function(err, post){
      if(err){console.log(err);}
      var sasuga = post.sasuga;
      post.sasuga = sasuga + 1;
      post.save(function(err){
        if(err){console.log(err);}
        res.json([true, ""]);
      });
    });
    res.send("update: called as PUT method");
  },
  new: function(req, res){
    res.send("new: called as GET method");
  },
  create: function(req, res){
    var data = req.body;
    var instance = new Blog();
    instance.user = data.name;
    instance.title = data.title;
    instance.content = data.content;
    instance.room = "wtBlog";
    instance.mode = data.mode;
    //instance.mode = "plainText";
    instance.sasuga = 0;
    instance.save(function(err){
      if(err){
        res.json([false, err]);
      }else{
        Notify("新規記事：[「"+ data.title +"」](http://192.168.5.56:3003/)");
        res.json([true, ""]);
      }
    });
  },
  show: function(req, res){
    res.send("show: called as GET method");
  },
  edit: function(req, res){
    res.send("edit: called as GET method");
  },
  update: function(req, res){
    var data = req.body;
    return Blog.findOne({_id : new ObjectId(req.params.blog)}, function(err, old){
      old._id = mongoose.Types.ObjectId();
      old.deleted = new Date();
      return Blog.create(old, function(err){
        return Blog.findOneAndUpdate( {_id : new ObjectId(req.params.blog)},
          {$set : {name: data.user, title: data.title, content: data.content, mode: data.mode}},
        function(err, post){
          if(err){console.log(err);}
          res.json([true, ""]);
        });
      });
    });

    res.send("update: called as PUT method");
  },
  destroy: function(req, res){
    return Blog.findOneAndUpdate( {_id : new ObjectId(req.params.blog)},
      {$set : {deleted: new Date()}},
    function(err, post){
      if(err){console.log(err);}
      res.json([true, ""]);
    });
  }
};
